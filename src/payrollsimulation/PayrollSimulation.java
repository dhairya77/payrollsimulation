/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package payrollsimulation;

import java.util.List;

/**
 *
 * @author DHAIRYA
 */
public class PayrollSimulation {

  
        
        private List<employee> Employee;
        private List<manager> Manager;
        private String name;

    public List<manager> getManager() {
        return Manager;
    }

    public void setManager(List<manager> Manager) {
        this.Manager = Manager;
    }

    public PayrollSimulation(List<employee> Employee, List<manager> Manager, String name, String hourlywage) {
        this.Employee = Employee;
        this.Manager = Manager;
        this.name = name;
        this.hourlywage = hourlywage;
    }

    public PayrollSimulation(List<employee> Employee, String name, String hourlywage) {
        this.Employee = Employee;
        this.name = name;
        this.hourlywage = hourlywage;
    }

    public List<employee> getEmployee() {
        return Employee;
    }

    public void setEmployee(List<employee> Employee) {
        this.Employee = Employee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHourlywage() {
        return hourlywage;
    }

    public void setHourlywage(String hourlywage) {
        this.hourlywage = hourlywage;
    }
        private String hourlywage;
        // TODO code application logic here
    
    
}
