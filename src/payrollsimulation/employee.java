/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package payrollsimulation;

/**
 *
 * @author DHAIRYA
 */
public class employee {
    
    private String name;
    private String hourlywage;
    private String calculatePay;

    public employee(String name, String hourlywage, String calculatePay) {
        this.name = name;
        this.hourlywage = hourlywage;
        this.calculatePay = calculatePay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHourlywage() {
        return hourlywage;
    }

    public void setHourlywage(String hourlywage) {
        this.hourlywage = hourlywage;
    }

    public String getCalculatePay() {
        return calculatePay;
    }

    public void setCalculatePay(String calculatePay) {
        this.calculatePay = calculatePay;
    }
    
}
